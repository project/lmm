# Usage

Add the mapping to the library definition in your module's `libraries.yml` file as shown below

```yaml
global:
  version: 1.x
  css:
    theme:
      dist/app.css: { manifest: dist/manifest.json }
  js:
    dist/app.js: { manifest: dist/manifest.json }
  dependencies:
    - core/jquery
    - core/drupal
    - core/drupalSettings
    - core/once
```
